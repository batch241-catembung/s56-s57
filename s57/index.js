function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if (age <= 12) {
        return undefined;
    }
    if (age >= 13 && age <= 21) {
        let discount = price * 0.20;
        // let discountedPrice = Math.round(price - discount)
        let discountedPrice =  Math.round(price - discount);
        return discountedPrice.toString();
    }
    if (age >= 22 && age <= 64) {
        let roundedPrice = Math.round(price);
        return roundedPrice.toString();
    }

}